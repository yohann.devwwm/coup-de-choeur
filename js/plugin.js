(function($) {
  
  $.fn.coul = function(couleur){
    $('.lien-menu').hover(
      function(){
        $(this).css('background-color', couleur);
      },
      function(){
        $(this).css('background-color', '');
      });
      $('.fermer').hover(
        function(){
          $(this).css('background-color', couleur);
        },
        function(){
          $(this).css('background-color', '');
        });
    $('.deroulement').css('background-color', couleur);
    $('#chef').css('background-color', couleur);
    $('#programme').css('background-color', couleur);
    $('.carre-coul h3').css('color', couleur);
    $('#date').css('background-color', couleur);
    $('.lieu p').css('color', couleur);
    $('.coord h3').css('color', couleur);
    $('footer').css('background-color', couleur);
  };

  $.fn.coulfond = function(couleur){
    $('body').css('background-color', couleur);
    $('.menu').css('background-color', couleur);
    $('.carre-coul').css('background-color', couleur);
  };

  $.fn.coultext = function(couleur){
    $('body').css('color', couleur);
    $('.menu a').css('color', couleur);
    $('.fermer').css('color', couleur);
  };
})(jQuery);
