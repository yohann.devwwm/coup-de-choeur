$(function() {


// site pour code couleurs: https://www.w3schools.com/colors/colors_picker.asp
//Pour changer la couleur, modifier ce qu'il y a entre guillemets apres le #
  $.fn.coul('#D4B003');

// pour modifier la couleur de fond, modifier ce qu'il y a entre guillemets apres le #
  $.fn.coulfond('#FFFFFF');

// pour modifier la couleur du texte, modifier ce qu'il y a entre guillemets apres le #
  $.fn.coultext('#000000');

// pour modifier la couleur de horraires programme, modifier ce qu'il y a entre guillemets apres le #
  $('#date span').css('color','#FFFFFF');

// pour modifier la couleur des compositeurs, modifier ce qu'il y a entre guillemets apres le #
  $('.carre-coul h2').css('color','#84CBCB');





/*##################################################################################
####################################################################################
Ne pas modifier ci-dessous!!!!!!
####################################################################################
##################################################################################*/


    //@ quand on clic sur le boutton
      //@@ on ouvre le menu
    $('.deroulement').click(function(){
        $('.deroulement').css('display','none')
        var haut = $(document).height();
        var lar = $(document).width();
        $('.menu').height(haut)
        $('.menu').width(lar)
    });
    //@ quand on clic sur le boutton
      //@@ on ferme le menu
      $('.fermer').click(function(){
          $('.deroulement').css('display','block')
          $('.menu').height(0)
          $('.menu').width(0)
      });
      //@ quand on clic sur un lien
        //@@ on ferme le menu
      $('.lien-menu').click(function(){
          $('.deroulement').css('display','block')
          $('.menu').height(0)
          $('.menu').width(0)
      });
});
